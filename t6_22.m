clear;
clc;
%--------------------------------------------------------------%
% v = randseq(35,'alphabet','dna');
% w = randseq(10,'alphabet','dna');

v = fileread('Data/NC_006088.4.txt');
w = fileread('Data/NC_006088.5.txt');

max_score = 0;
position = -1;
% For every element of the big sequence
for i = 0:length(v)
    temp_score = 0;
    % For every letter  of the small sequence
    for j = 1:length(w)
        % Check if the position of the small sequence 
        % exceeds the size of the big sequence
        if i+j+1>length(v)
            break;
        end
        if v(i+j) == w(j)
            temp_score = temp_score + 1;
        end
    end
    if temp_score > max_score
        max_score = temp_score;
        position = i;
    end
end

% Display result
prompt1 = 'Max score is: ' + string(max_score);
disp(prompt1);
prompt2 = 'Position of the first occurance is: ' + ...
    string(position);
disp(prompt2);
% Part of the v that matches with w
matching_v = v(position+1:position+length(w));

% Remove disagreements in the beginning
while matching_v(1) ~= w(1)
    matching_v = matching_v(2:end);
    w = w(2:end);
end

% Remove disagreements at the end
while matching_v(end) ~= w(end)
    matching_v = matching_v(1:end-1);
    w = w(1:end-1);
end

% Set the relations
relations = [];
for i=1:length(w)
    if matching_v(i) == w(i)
        relations = [relations,'| '];
    else
        relations = [relations,'X'];
    end
end
disp(matching_v);
disp(relations);
disp(w);
