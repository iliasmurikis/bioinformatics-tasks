clear;
clc;
%--------------------------------------------------------------------%
seqn = randseq(25,'alphabet','dna');
data1 = fileread('AC_000162.txt');
data2 = getgenbank('NC_037332','PartialSeq',[31184171,31186192]);

% seqn = data2;
n = length(seqn);
disp(length(seqn));
disp(seqn);

% First move, player1 removes x elements
x = mod(n,3);
if x ~=0
    seqn = seqn(1:end-x);
    disp('Player 1, played');
    disp(length(seqn));
    disp(seqn);
end

% Rest of the game
while ~isempty(seqn)
    prompt = 'Player 2, choose to remove 1 or 2:';
    choice  = input(prompt);
    seqn = seqn(1:end-choice);
    disp('Player 2, played');
    disp(length(seqn));
    disp(seqn);
    % My players move
    seqn = seqn(1:end-(3-choice));
    disp('Player 1, played');
    disp(length(seqn));
    disp(seqn);
end

disp('End')