clear;
clc;
%--------------------------------------------------------------------%
seq = randseq(25,'alphabet','dna');
data1 = fileread('Data/AC_000162.txt');
data2 = getgenbank('NC_037332','PartialSeq',[31184171,31186192]);

% seq = data2;
n = length(seq);
disp(length(seq));
disp(seq);

% Check the first elements which are the last that would be removed
i=2;
temp = seq(1);
while  true
    if seq(i) ~=temp
        break;
    end
    i = i + 1;
end
% Create the new sequence
new_seq = seq(i:end);

x = mod(new_seq,3);
if x~=0
    % I play
    new_seq = new_seq(1:end-x);
    disp(new_seq)
    disp('-------------------------------------------------------------------');
end

% Otherwise he plays first
while ~isempty(new_seq) 
    prompt = 'Player2, choose to remove 1 or 2 nucleotide(s):';
    choice  = input(prompt);
    if choice == 1
        new_seq = new_seq(1:end-1);
    elseif choice== 2
        new_seq = new_seq(1:end-2);
    end
    disp(new_seq)
    % My turn
    if choice == 1
        ne_seq = new_seq(1:end-2);
    elseif choice== 2
        new_seq = new_seq(1:end-1);
    end
    disp(new_seq);
    disp('-------------------------------------------------------------------');
end

disp('End');
