clear;
clc;
%--------------------------------------------------------------%
% Create the v sequence
size_of_v = 25;
v = randseq(size_of_v,'alphabet','dna');

N = 10;

% Creating the w sequences
size_of_ws = ceil(size_of_v/3);
w = [];
for i=1:N
    seq = randseq(size_of_ws,'alphabet','dna');
    w = [w,string(seq)];
end


% Create the matrix for i,j and score
scores = zeros(1,3);

% Calculate all the possible scores
for i=1:N
    for j = 1:N
        ij = w(i)+w(j);
        score = nwalign(v,ij);
        % Keeping the best
        if score>scores(3)
            scores(1) = i;
            scores(2) = j;
            scores(3) = score;
        end
    end
end

% Displaying the sequences
v
w(fix(scores(1)))
w(fix(scores(2)))

prompt = 'Sequences w'+string(fix(scores(1)))+' and w'+string(fix(scores(2)))+' alligned best with the sequnce v. \nEnd the score is: '+string(scores(3))+'\n';
fprintf(prompt);