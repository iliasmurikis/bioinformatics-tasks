clear;
clc;
%--------------------------------------------------------------%

one_bbt_1 = fileread('Data/1BBT_1.txt');
one_bbt_1 = one_bbt_1(1:end-1);

% i) Amino-acid Sequence
for i = 1:length(one_bbt_1)
    if mod(i,3) == 0
        fprintf(one_bbt_1(i));
        fprintf('-');
    end
    fprintf(one_bbt_1(i));    
end
fprintf('\n');

% ii) Nucleotide Sequence
nucleotide = aa2nt(one_bbt_1);
disp(nucleotide);
