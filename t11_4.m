clear;
clc;
%--------------------------------------------------------------%
% Dictionary
d = 'ACGT';

x_old = 'GGCT';
% Convert protein into numbers
x = [];
for i =1:length(x_old)
    if x_old(i) == d(1)
        x = [x,1]; %#ok<*AGROW>
    elseif x_old(i) == d(2)
        x = [x,2];
    elseif x_old(i) == d(3)
        x = [x,3];
    elseif x_old(i) == d(4)
        x = [x,4];
    else
        disp('Invalid input');
        exit();
    end
end
% Chances of states
A = [0.9 0.1; 0.1 0.9];
% Chances of emiting
B = [0.4 0.4 0.1 0.1; 0.2 0.2 0.3 0.3]';
% Initial chances for begin state
pi_init = [0.5; 0.5];

% Number of states
N = size(A,1);
% Number of nucleotids
M = size(B,1);
% Length of the sequence
T = length(x);

% Create score matrix
c = zeros(N,T);

% Create an array of the paths that followed
paths = zeros(N,T);

% Initialization
for i = 1:N
    c(i,1) = log10(pi_init(i))+log10(B(x(1),i));
end

% Iteration
for t = 2:T      % for every number of the sequence
                      % 2nd to the last
    for i = 1:N    % for every state 
        % N different scores, in which I choose the greatest
        v = zeros(1,N);
        for p = 1:N    % from each predecessor
            v(p)  = c(p,t-1) + A(p,i) * B(x(t),i);
        end
        [c(i,t),ind] = max(v);
        paths(i,t)  = ind;
    end   
end

% Find best route
[~,bp] = max(c(:,end));
for t = T:-1:2
    ind = paths(bp(end),t);
    bp = [bp,ind];
end

% Switch the order
bp = fliplr(bp);

% Find Viterbi score
score = 1;
for t=1:T
    score = score * c(bp(t),t);
end

% Convert numbers to state names
sequence = [];
for i = 1:T
    if bp(i) == 1
        sequence = [sequence,'a'];
    elseif bp(i) == 2
        sequence = [sequence,'b'];
    end   
end

% Output the result
prompt  = 'States sequence is: ' + string(sequence);
disp(prompt);