clear;
clc;
%--------------------------------------------------------------%
x = [1,1,2,1,2,2];
% Chances of states
A = [0.5 0.25; 0.25 0.5];
% Chances of emiting
B = [0.5 0.25 0.25; 0.25 0.5 0.25]';
% Chances to go in the End state
E = [0.25 0.25];
% Initial chances for begin state
pi_init = [0.5; 0.5];

% Number of states
N = size(A,1);
% Number of nucleotids
M = size(B,1);
% Length of the sequence
T = length(x);

% Create score matrix
c = zeros(N,T+1);

% Create an array of the paths that followed
paths = zeros(N,T+1);
paths2 = zeros(N,T+1);

% Initialization
for i = 1:N
    c(i,1) = log2(pi_init(i))+log2(B(x(1),i));
end

% Iteration
for t = 2:T      % for every number of the sequence
                      % 2nd to the last
    for i = 1:N    % for every state 
        % N different scores, in which I choose the greatest
        v = zeros(1,N);
        for p = 1:N    % from each predecessor
            v(p)  = c(p,t-1) + A(p,i) + B(x(t),i);
        end
        [c(i,t),ind] = max(v);
        paths(i,t)  = ind;
        % Checks if there are more than one paths
        if v(1) == v(2)
            paths2(i,t) = 1+2;
        end
    end   
end

% Calculate End-state score
for i = 1:N
    c(i,T+1)  = c(i,T) + E(i);
    paths(i,T+1) = i;
end

% Find best route
[~,bp] = max(c(:,end));
for t = T+1:-1:2
    ind = paths(bp(end),t);
    bp = [bp,ind];
end

% Switch the order
bp = fliplr(bp);


% Find Viterbi score
score = 1;
for t=1:T+1
    score = score * c(bp(t),t);
end

% Convert numbers to state names
sequence = 'Start -';
for i = 1:T
    if bp(i) == 1
        sequence = [sequence,' D1 -'];
    elseif bp(i) == 2
        sequence = [sequence, ' D2 -'];
    end   
end
sequence = [sequence,' End'];

prompt  = 'States sequence is: ' + string(sequence);
disp(prompt);
prompt = 'Score is: ' + string(score);
disp(prompt);